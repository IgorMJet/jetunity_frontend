import { mapGetters } from 'vuex'
import { fetchPage } from '../utils/asyncPages'
import { pageMeta } from '../utils/pageMeta'

export const pageMixin = {
  computed: {
    ...mapGetters({
      settings: 'vars/settings'
    })
  },

  async created () {
    this.parseResponse()
  },

  data () {
    return {
      metaDescription: '',
      metaKeywords: '',
      metaTitle: ''
    }
  },

  async fetch ({ app }) {
    try {
      return app.store.dispatch('vars/settings')
    } catch (e) { console.log(e) }
  },

  head () {
    return pageMeta(this)
  },

  methods: {
    // если требуется перегрузка данных страницы на открытой странице
    async fetchPage (url) {
      const vm = this

      // eslint-disable-next-line no-async-promise-executor
      return new Promise(async (resolve, reject) => {
        try {
          const result = await fetchPage(this, () => {
            // eslint-disable-next-line prefer-promise-reject-errors
            reject(vm)
            return vm.$router.push({ name: 'errors-404' })
          }, url)

          if (result.fetchError) {
            // eslint-disable-next-line prefer-promise-reject-errors
            reject(vm)
            return vm.$router.push({ name: 'errors-404' })
          }

          for (const key in result) {
            if (Object.prototype.hasOwnProperty.call(result, key)) {
              vm[key] = result[key]
            }
          }

          vm.parseResponse()
          resolve(vm)
        } catch (error) {
          // eslint-disable-next-line prefer-promise-reject-errors
          reject(vm)
          vm.$router.push({ name: 'errors-404' })
        }
      })
    },

    onError () {},
    onSuccess () {},

    parseResponse () {
      if (this.fetchError) {
        this.onError()
        return false
      }

      this.$nextTick(() => {
        if (this.page && this.page.title) {
          this.metaTitle = this.page.title
        } else if (typeof (this.title) !== 'undefined' && this.title) {
          this.metaTitle = this.title
        }

        let meta = this.meta || null
        if (this.page && this.page.meta) {
          meta = this.page.meta
        }

        if (meta && meta.seo_description) {
          this.metaDescription = meta.seo_description
        }
        if (meta && meta.seo_keywords) {
          this.metaKeywords = meta.seo_keywords
        }
        if (meta && meta.seo_title) {
          this.metaTitle = meta.seo_title
        }

        this.onSuccess()
      })
    },

    prepareTitle (title) {
      if (this.settings && (this.settings.SEO_TITLE_PREFIX || this.settings.SEO_TITLE_POSTFIX)) {
        const prefix = this.settings.SEO_TITLE_PREFIX || ''
        const postfix = this.settings.SEO_TITLE_POSTFIX || ''
        return `${prefix} ${title} ${postfix}`
      } else {
        return title
      }
    }
  }
}
