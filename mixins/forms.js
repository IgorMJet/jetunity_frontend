import { Alert, errorResponseAlert } from '@/utils/alert'
import { getLocalStorageItem } from '@/utils/localStorage'

export const formMixin = {
  data () {
    return {
      errorMessages: null
    }
  },

  methods: {
    clearServerError (field) {
      if (this.errorMessages && field in this.errorMessages) {
        this.errorMessages[field] = null
      }
    },

    parseFormErrors (error, title, forceAlert) {
      title = title || this.$t('error')
      forceAlert = forceAlert || false

      this.errorMessages = null
      if (!error.response) {
        return Alert(title, error)
      }

      if (!forceAlert && error.response.data.detail && error.response.data.detail.errors) {
        if (Array.isArray(error.response.data.detail.errors)) {
          const errorMessages = {}
          error.response.data.detail.errors.forEach(e => {
            errorMessages[e.name] = [e.message]
          })
          this.errorMessages = errorMessages
        } else {
          this.errorMessages = error.response.data.detail.errors
        }
      } else {
        errorResponseAlert(error.response, title)
      }
    },

    setUTMs (formData) {
      formData.utm_source = getLocalStorageItem('utm_source') || null
      formData.utm_medium = getLocalStorageItem('utm_medium') || null
      formData.utm_campaign = getLocalStorageItem('utm_campaign') || null
      formData.utm_content = getLocalStorageItem('utm_content') || null
      formData.utm_term = getLocalStorageItem('utm_term') || null
    },

    touchField (field) {
      if (this.$v && this.$v.form) {
        const path = field.split('.')
        field = this.$v.form
        path.forEach(p => {
          field = field[p]
        })
        if (field) {
          field.$touch()
        }
      }
    }
  }
}
