import { mapGetters } from 'vuex'
import { removeLocalStorageItem, setLocalStorageItem } from '@/utils/localStorage'

export const layoutMixin = {
  computed: {
    ...mapGetters({
      menus: 'vars/menus',
      settings: 'vars/menus'
    })
  },

  mounted () {
    if (this.$route) {
      const q = this.$route.query || {}
      if (q && (q.r || q.R)) {
        setLocalStorageItem('referral', q.r || q.R)
      }

      if (q.utm_source) {
        const utms = ['utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term']
        utms.forEach(utm => {
          removeLocalStorageItem(utm)
          const value = q[utm] || null
          if (value) {
            setLocalStorageItem(utm, value)
          }
        })
      }
    }
  }
}
