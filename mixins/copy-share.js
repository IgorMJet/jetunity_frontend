import { Alert } from '@/utils/alert'
import { mapGetters } from 'vuex'
import { getBaseUrl } from '@/utils/baseUrl'
import config from '@/config'

export const copyShareMixin = {
  computed: {
    ...mapGetters({ settings: 'vars/settings' }),

    allowShare () {
      return navigator && !!navigator.share
    },

    referralLink () {
      if (this.successMessage && this.successMessage.referral_code && this.baseURL) {
        const scope = !!this.referralToMain || config.theme === 'skytec' ? '' : 'brokers'
        return `${this.baseURL}/${scope}?r=${this.successMessage.referral_code}`
      }

      return null
    },

    shareOptions () {
      return [{
        text: 'Facebook',
        value: 'facebook'
      }, {
        text: 'VK',
        value: 'vk'
      }, {
        text: 'Twitter',
        value: 'twitter'
      }]
    }
  },

  data () {
    return {
      baseURL: '',
      isShareDropdownShown: false
    }
  },

  methods: {
    copyStringToClipboard () {
      const str = this.$refs.refInput.value
      const el = document.createElement('textarea')
      el.value = str
      el.setAttribute('readonly', true)
      el.style.cssText = 'position: absolute; left: -9999px'
      document.body.appendChild(el)
      el.select()
      document.execCommand('copy')
      document.body.removeChild(el)
      Alert(this.$t('clipboard.copied'))
    },

    share () {
      const errorTitle = this.$t('error')
      const errorContent = this.$t('errors.errorOccured')
      navigator.share({
        title: this.settings.share_title || '',
        text: this.settings.share_content || '',
        url: this.referralLink
      })
        .then(() => console.log('Successful share'))
        .catch((error) => {
          console.log('Error sharing', error)
          Alert(errorTitle, errorContent)
        })
    },

    sharePopup (url) {
      if (process.browser) {
        const maxWidth = Math.min(window.outerWidth, 650)
        const maxHeight = Math.min(window.outerHeight, 420)
        window.open(url, '', `toolbar=0,status=0,width=${maxWidth},height=${maxHeight}`)
      }
    },

    shareSocial (network) {
      if (process.server) {
        return
      }
      const shareUrl = encodeURIComponent(this.referralLink)
      const space = encodeURIComponent(' ')
      const title = encodeURIComponent(this.settings.share_title || '')
      const text = encodeURIComponent(this.settings.share_content || '')
      let url = null

      if (network === 'facebook') {
        url = `http://www.facebook.com/sharer/sharer.php?u=${shareUrl}`
      }
      if (network === 'twitter') {
        url = `https://twitter.com/share?url=${shareUrl}&counturl=${shareUrl}&text=${title}${space}${text}`
      }
      if (network === 'vk') {
        url = `https://vk.com/share.php?url=${shareUrl}&title=${title}&description=${text}&noparse=true`
      }

      if (url) {
        this.sharePopup(url)
      }
    }
  },

  mounted () {
    this.baseURL = getBaseUrl()
  }
}
