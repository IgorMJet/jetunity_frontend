export default {
  baseURL: 'https://api.jet.space/api/',
  mediaURL: 'https://api.jet.space/media',
  metrikaID: 66798508,
  locales: ['en', 'ru'],
  siteID: 1,
  theme: 'jetunity' // 'skytec'
}
