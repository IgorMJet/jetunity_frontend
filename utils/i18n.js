import { getLocalStorageItem, setLocalStorageItem } from './localStorage'
import config from '../config'

export const LOCALE_STORAGE_KEY = 'locale'

export function changeLocale (locale, app, store) {
  if (testLocale(locale)) {
    store.commit('SET_LOCALE', locale)
    app.i18n.locale = locale
    saveLocale(locale)
  }
}

export function testLocale (locale) {
  return locale && config.locales.indexOf(locale.toLowerCase()) >= 0
}

export function getLocale (req) {
  let locale = null

  if (process.browser && typeof (window) !== 'undefined') {
    const urlParts = window.location.pathname.split('/')
    const urlLocale = urlParts.length > 1 ? urlParts[1] : ''
    if (config.locales.indexOf(urlLocale) >= 0) {
      locale = urlLocale
    }
  }

  if (!locale && process.browser) {
    try {
      locale = getLocalStorageItem(LOCALE_STORAGE_KEY) || null
      // eslint-disable-next-line no-empty
    } catch (e) {}
  }

  if (!testLocale(locale)) {
    locale = config.locales[0]
  }

  return locale ? locale.toLowerCase() : config.locales[0]
}

export function saveLocale (locale) {
  if (config.locales.indexOf(locale) >= 0) {
    setLocalStorageItem(LOCALE_STORAGE_KEY, locale)
  }
}
