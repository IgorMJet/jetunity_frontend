import { getLocale } from './i18n'

export const pageMeta = (vm, cards) => {
  cards = typeof (cards) === 'undefined' ? true : cards

  const head = {
    htmlAttrs: { lang: vm.$store.state.locale || getLocale() },
    title: vm.prepareTitle(vm.metaTitle),
    meta: [
      { hid: 'description', name: 'description', content: vm.metaDescription },
      { hid: 'keywords', name: 'keywords', content: vm.metaKeywords }
    ]
  }

  if (cards) {
    head.meta.push(
      { hid: 'og:title', name: 'og:title', content: vm.metaTitle },
      { hid: 'og:description', name: 'og:description', content: vm.metaDescription },
      { hid: 'twitter:title', name: 'twitter:title', content: vm.metaTitle },
      { hid: 'twitter:description', name: 'twitter:description', content: vm.metaDescription }
    )
  }

  return head
}
