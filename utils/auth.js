import { getCookie, removeCookie, setCookie } from './cookies'
const TOKEN_NAME = 'token'
export const COOKIE_TOKEN_LIFETIME = (15 * 24 * 60 * 60) - 60

export const getToken = (req) => {
  const token = getCookie(TOKEN_NAME, req) || ''
  const hasStorage = typeof (localStorage) !== 'undefined' && localStorage.getItem && localStorage.setItem
  if (token && hasStorage) {
    localStorage.setItem(TOKEN_NAME, token)
  }
  return hasStorage ? localStorage.getItem(TOKEN_NAME) || '' : token
}

export const setToken = (token) => {
  if (process.server) {
    return
  }

  if (typeof (localStorage) !== 'undefined' && localStorage.setItem) {
    localStorage.setItem(TOKEN_NAME, token)
  }

  setCookie(TOKEN_NAME, token, { expires: COOKIE_TOKEN_LIFETIME })
}

export const unsetToken = () => {
  if (process.server) {
    return
  }

  if (typeof (localStorage) !== 'undefined' && localStorage.removeItem) {
    localStorage.removeItem('token')
  }
  removeCookie(TOKEN_NAME)
}
