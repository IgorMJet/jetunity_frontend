import eventHub from '../utils/eventHub'

export const Alert = (title, content, i18n) => {
  eventHub.$emit('global:alert', { title, content, i18n: i18n || false })
}

export const Confirm = (title, content, callback, i18n) => {
  eventHub.$emit('global:confirm', { title, content, callback, i18n: i18n || false })
}

export const errorResponseAlert = (response, msg, i18n) => {
  if (!msg) {
    msg = ''
  }

  if (typeof (response.data) === 'undefined') {
    response.data = {}
  }

  console.log(`${msg}:`)
  console.log(response.data)
  const data = response.data

  if (typeof (data.detail) !== 'undefined' && data.detail) {
    let detail = data.detail

    if (detail.errors) {
      const errors = detail.errors
      detail = []
      if (Array.isArray(errors)) {
        errors.forEach(value => {
          let message = value.message
          if (value.name === '__all__') {
            message = value.message
          }
          detail.push(message)
        })
      }
      detail = detail.join('<br>')
    }

    eventHub.$emit('global:alert', {
      content: detail,
      title: msg,
      i18n: i18n || false
    })
  } else {
    eventHub.$emit('global:alert', {
      content: data,
      title: msg,
      i18n: i18n || false
    })
  }
}
