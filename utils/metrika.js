import config from '../config'

export function getMetrikaCounter () {
  if (process.browser && typeof (window.ym) !== 'undefined') {
    return window.ym
  }
  return null
}

export function execMetrikaMethod (method, args, attempts) {
  // console.log(method, args)

  attempts = attempts || 0
  const counter = getMetrikaCounter()
  if (counter) {
    counter(config.metrikaID, method, ...args)
  } else {
    if (process.browser && attempts < 1) {
      document.addEventListener('yacounter' + config.metrikaID + 'inited', () => {
        execMetrikaMethod(method, args, attempts + 1)
      })
    }
  }
}
