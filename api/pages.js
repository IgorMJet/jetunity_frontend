export default {
  contacts () { return 'pages/contacts/' },
  flatpages () { return 'pages/flatpages/' },
  flatpage (slug) { return `pages/flatpages/${slug}/` },
  homepage () { return 'pages/homepage/' }
}
