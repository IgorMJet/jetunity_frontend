import brokers from './brokers'
import forms from './forms'
import jetsharing from './jetsharing'
import pages from './pages'
import referrals from './referrals'
import seo from './seo'
import services from './services'
import vars from './vars'

export default {
  brokers,
  forms,
  jetsharing,
  pages,
  referrals,
  seo,
  services,
  vars
}
