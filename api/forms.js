export default {
  booking () { return 'forms/booking/' },
  brokers () { return 'forms/brokers/' },
  jetSharingBooking () { return 'forms/jetsharing/booking/' },
  partnership () { return 'forms/partnership/' },
  userReg () { return 'forms/user/' }
}
