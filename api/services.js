export default {
  servicesPage () { return 'services/page/' },
  services () { return 'services/services/' },
  service (slug) { return `services/services/${slug}/` }
}
