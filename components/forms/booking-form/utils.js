export const renderAirport = airport => {
  return `${airport.city ? airport.city.title + ', ' : ''}` +
    `${airport.title || ''}${airport.title && airport.code ? ', ' : ''}${airport.code}`
}
