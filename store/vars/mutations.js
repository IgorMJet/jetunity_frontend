import zipObject from 'lodash/zipObject'

const SET_SETTINGS = (state, payload) => {
  state.settings = (payload || {}).settings || {}
  const menus = (payload || {}).menus || []
  const menuKeys = menus.map(o => o.slug)
  state.menus = zipObject(menuKeys, menus)
}

const SET_SETTINGS_FAIL = (state) => {
  state.menus = null
  state.settings = null
}

export default {
  SET_SETTINGS,
  SET_SETTINGS_FAIL
}
