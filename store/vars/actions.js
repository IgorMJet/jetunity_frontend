import API from '../../api'

function settings ({ state, commit }) {
  return new Promise((resolve, reject) => {
    if (state.settings !== null) {
      resolve(state)
      return
    }

    this.$axios
      .get(API.vars.settings())
      .then(response => {
        commit('SET_SETTINGS', response.data)
        resolve(response)
      })
      .catch(({ response }) => {
        commit('SET_SETTINGS_FAIL', null)
        reject(response)
      })
  })
}

export default { settings }
