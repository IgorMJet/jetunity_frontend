import config from './config'
const axios = require('axios')
const cloneDeep = require('lodash/cloneDeep')

module.exports = {
  axios: {
    baseURL: config.baseURL,
    browserBaseURL: config.baseURL,
    https: process.env.NODE_ENV === 'production',
    proxy: true,
    proxyHeaders: true
  },

  build: {
    analyze: false,
    cssSourceMap: true,
    cache: false,
    extend (config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }

      if (ctx.isDev) {
        config.devtool = 'eval-source-map'
      }

      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'))
      svgRule.test = /\.(png|jpe?g|gif|webp)$/

      config.module.rules.push({
        test: /\.svg$/,
        loader: 'vue-svg-loader',
        options: {
          useSvgo: false // default: true
        }
      })
    },
    extractCSS: true,
    html: {
      minify: {
        collapseBooleanAttributes: true,
        collapseWhitespace: true,
        decodeEntities: true,
        minifyCSS: true,
        minifyJS: true,
        processConditionalComments: true,
        removeEmptyAttributes: true,
        removeRedundantAttributes: true,
        trimCustomFragments: true,
        useShortDoctype: true
      }
    },
    postcss: {
      plugins: {
        lost: {},
        'postcss-center': {},
        'postcss-short': {},
        'postcss-flexbox': {},
        'postcss-flexbugs-fixes': {},
        'postcss-media-minmax': {},
        'postcss-aspect-ratio': {},
        'postcss-responsive-type': {},

        // 'postcss-normalize': {
        //   forceImport: true
        // },
        autoprefixer: {
          overrideBrowserslist: ['last 2 versions', 'ie >= 9']
        }
      }
    },
    ssr: true,
    transpile: [/^vue2-google-maps($|\/)/],
    uglify: {
      uglifyOptions: {
        compress: true,
        mangle: { safari10: true }
      }
    }
  },

  css: [
    { src: './node_modules/@hokify/vuejs-datepicker/dist/vuejs-datepicker.css', lang: 'css' },
    { src: './node_modules/fullpage.js/dist/fullpage.css', lang: 'css' },
    { src: './node_modules/choices.js/src/styles/choices.scss', lang: 'scss' },
    { src: './node_modules/perfect-scrollbar/css/perfect-scrollbar.css', lang: 'css' },
    { src: './node_modules/nouislider/distribute/nouislider.css', lang: 'css' },
    { src: './node_modules/sanitize.css/sanitize.css', lang: 'css' },
    { src: './node_modules/swiper/css/swiper.css', lang: 'css' },
    { src: './node_modules/video.js/dist/video-js.css', lang: 'css' },
    { src: '~/styles/index.styl', lang: 'stylus' }
  ],

  dev: process.env.NODE_ENV !== 'production',

  head: {
    bodyAttrs: {
      class: [`theme-${config.theme || 'jetunity'}`]
    },
    htmlAttrs: { lang: config.locales[0] },
    link: [
      { rel: 'shortcut icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'manifest', href: '/manifest.json' },
      { rel: 'apple-touch-icon', href: `/icons/${config.theme || 'jetunity'}/apple-touch-icon.png`, sizes: '180x180' },
      { rel: 'icon', href: `/icons/${config.theme || 'jetunity'}/favicon-32x32.png`, sizes: '32x32', type: 'image/png' },
      { rel: 'icon', href: `/icons/${config.theme || 'jetunity'}/favicon-16x16.png`, sizes: '16x16', type: 'image/png' }
    ],
    meta: [
      { charset: 'utf-8' },
      { hid: 'viewport', name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'skype-toolbar', content: 'skype-toolbar-parser-compatible' },
      { hid: 'description', name: 'description', content: '' },
      { hid: 'keywords', name: 'keywords', content: '' },
      { hid: 'msapplication-config', name: 'msapplication-config', content: '/browserconfig.xml' },
      { hid: 'msapplication-TileImage', name: 'msapplication-TileImage', content: `/icons/${config.theme || 'jetunity'}/mstile-150x150.png` },
      { hid: 'msapplication-TileColor', name: 'msapplication-TileColor', content: '#000000' },
      { hid: 'theme-color', name: 'theme-color', content: '#000000' },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:locale', property: 'og:locale', content: 'ru_RU' },
      { hid: 'twitter:card', property: 'twitter:card', content: 'summary_large_image' },
      { hid: 'twitter:domain', property: 'twitter:domain', content: 'jetunity.com' }
    ],
    title: 'BoardMaps'
  },

  loading: {
    color: '#888',
    failedColor: '#ff0000',
    thickness: '2px',
    transition: {
      speed: '0.2s',
      opacity: '0.6s'
    },
    autoRevert: true
  },

  manifest: {
    name: 'BoardMaps',
    lang: config.locales[0]
  },

  mode: 'universal',

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/sitemap',
    '@nuxtjs/style-resources',
    'nuxt-fullpage.js',
    ['vue-yandex-maps/nuxt',
      {
        apiKey: '',
        lang: 'ru_RU',
        version: '2.1'
      }
    ]],

  styleResources: {
    stylus: [
      './node_modules/rupture/rupture/index.styl',
      './styles/index.styl',
      './styles/config.styl',
      './styles/vars.styl', // global variables
      `./styles/vars-${config.theme || 'jetunity'}.styl`, // theme specific variables
      './styles/mixins.styl',
      './styles/base.styl',
      `./styles/fonts-${config.theme || 'jetunity'}.styl`,
      './styles/icons.styl',
      './styles/transitions/**/*.styl',
      './styles/main.styl',
      './components/**/*.styl',
      './styles/pages/**/*.styl',
      `./styles/theme-${config.theme || 'jetunity'}.styl`
    ]
  },

  plugins: [
    '~/plugins/axios',
    '~/plugins/excluded-tags',
    '~/plugins/global-components',
    '~/plugins/i18n',
    '~/plugins/inputmask',
    '~/plugins/map',
    '~/plugins/raven',
    '~/plugins/router',
    '~/plugins/object-fit-polyfill',
    { src: '~/plugins/datepicker', ssr: false },
    { src: '~/plugins/scrollbar', ssr: false },
    { src: '~/plugins/fullpage', ssr: false },
    { src: '~/plugins/select', ssr: false },
    { src: '~/plugins/body-scroll-lock', ssr: false },
    { src: '~/plugins/observe-visibility', ssr: false },
    { src: '~/plugins/scrollto', ssr: false },
    { src: '~/plugins/intersection-observer-polyfill', ssr: false },
    { src: '~/plugins/swiper', ssr: false },
    '~/plugins/textarea'
  ],

  proxy: {
    '/api': {
      target: config.baseURL,
      pathRewrite: { '^/api': '' }
    },
    '/media': {
      target: config.mediaURL,
      pathRewrite: { '^/media': '' }
    }
  },

  render: {
    resourceHints: false,
    static: {
      maxAge: 1000 * 60 * 60 * 24 * 7
    }
  },

  router: {
    extendRoutes (routes, resolve) {
      const iterfunc = (nodes, clear) => {
        const result = nodes.slice()
        if (clear) {
          nodes.splice(0, nodes.length)
        }
        result.forEach(r => {
          if (r.name && r.name === 'index') {
            const locales = config.locales.slice(1)
            locales.forEach(l => {
              r = cloneDeep(r)
              r.path = `/${l}`
              r.name = 'index-' + l
              nodes.splice(0, 0, r)
            })
            return
          }

          r = cloneDeep(r)
          if (r.name) {
            r.name = 'locale-' + r.name
          }
          if (!clear && r.path) {
            r.path = '/:locale' + r.path
          }
          if (!!r.children && r.children.length) {
            iterfunc(r.children, true)
          }
          nodes.push(r)
        })
      }

      iterfunc(routes, false)
    },
    middleware: 'commonRedirects'
  },

  sitemap: {
    path: '/sitemap.xml',
    hostname: 'https://jetunity.com',
    cacheTime: process.env.NODE_ENV === 'production' ? 1000 * 60 * 10 : 100,
    gzip: false,
    generate: false,
    exclude: ['/appndx', '/errors/**'],
    async routes () {
      const query = `?limit=1000000&site_id=${1900 + new Date().getYear()}`
      const baseURL = `${config.baseURL}${config.locales[0]}/`
      const [flatpages] = await Promise.all([
        axios.get(baseURL + `pages/flatpages/${query}`)
      ])
      let res = []
      const defaultLocale = config.locales && config.locales.length ? config.locales[0] : null
      config.locales.forEach(locale => {
        locale = locale === defaultLocale ? '' : `/${locale}`

        res = res.concat([
          `${locale}`
        ])

        res = res.concat(flatpages.data.results.map(p => `${locale}/pages/${p.slug}`))
      })
      return res
    }
  }
}
