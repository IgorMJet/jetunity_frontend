export default {
  aboutInfo: 'Information about the company',
  all: 'All',
  booking: {
    addFlight: 'Add more flight',
    arrival: 'Arrival',
    departure: 'Departure',
    flightDate: 'Flight Date',
    flightTypes: {
      sharing: 'Sharing',
      shuttle: 'Shuttle'
    },
    flightsNotFound: 'Flights not found',
    freeSeats: 'Number of free seats',
    price: 'Price',
    wantReturnFlight: 'I want a return flight',
    yourData: 'Your data'
  },
  clipboard: {
    copied: 'Link copied'
  },
  cookie: {
    accept: 'Accept and continue'
  },
  error: 'Error',
  errors: {
    aboutCompany: 'Information about the company',
    errorOccured: 'An error has occurred',
    400: 'Invalid request',
    403: 'Forbidden',
    404: 'This page does not exist or has been deleted',
    500: 'Internal server error'
  },
  footer: {
    callUs: 'Call us',
    developedBy: 'Designed by'
  },
  forms: {
    buttons: {
      back: 'Back',
      book: 'Book',
      cancel: 'Cancel',
      close: 'Close',
      copy: 'Copy',
      ok: 'OK',
      resetFilter: 'Reset filter',
      search: 'Search',
      send: 'Send',
      share: 'Share'
    },
    errors: {
      email: 'Invalid email',
      maxValue: 'Max {0}',
      minValue: 'Min {0}',
      required: 'Required'
    },
    fields: {
      fullName: 'Full name',
      lastName: 'Last name',
      name: 'Name',
      phone: 'Phone number',
      seats: 'Number of seats'
    }
  },
  goBackToMain: 'Go back to the main page',
  moreDetails: 'More details',
  next: 'Next'
}
