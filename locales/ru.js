export default {
  aboutInfo: 'Информация о компании',
  all: 'Все',
  booking: {
    addFlight: 'Добавить ещё рейс',
    arrival: 'Куда',
    departure: 'Откуда',
    flightDate: 'Дата вылета',
    flightTypes: {
      sharing: 'Sharing',
      shuttle: 'Shuttle'
    },
    flightsNotFound: 'Рейсы не найдены',
    freeSeats: 'Количество свободных мест',
    price: 'Стоимость',
    wantReturnFlight: 'Хочу чтобы мне подобрали обратный рейс',
    yourData: 'Ваши данные'
  },
  clipboard: {
    copied: 'Ссылка скопирована'
  },
  cookie: {
    accept: 'Принять и продолжить'
  },
  error: 'Ошибка',
  errors: {
    aboutCompany: 'Информация о компании',
    errorOccured: 'Произошка ошибка',
    400: 'Неправильный запрос',
    403: 'Доступ запрещен',
    404: 'Такой страницы не существует или она была удалена',
    500: 'Внутренняя ошибка'
  },
  footer: {
    callUs: 'Позвоните нам',
    developedBy: 'Дизайн сайта'
  },
  forms: {
    buttons: {
      back: 'Назад',
      book: 'Бронировать',
      cancel: 'Отмена',
      close: 'Закрыть',
      copy: 'Скопировать',
      ok: 'ОК',
      resetFilter: 'Сбросить фильтр',
      search: 'Подобрать',
      send: 'Отправить',
      share: 'Поделиться'
    },
    errors: {
      email: 'Неправильный e-mail',
      maxValue: 'Максимум {0}',
      minValue: 'Минимум {0}',
      required: 'Обязательное поле'
    },
    fields: {
      fullName: 'Полное имя',
      lastName: 'Фамилия',
      name: 'Имя',
      phone: 'Телефон',
      seats: 'Количество мест'
    }
  },
  goBackToMain: 'Вернуться на главную',
  moreDetails: 'Подробнее',
  next: 'Далее'
}
