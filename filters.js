import format from 'date-fns/format'
import formatDuration from 'date-fns/formatDuration'
import parseISO from 'date-fns/parseISO'
import en from 'date-fns/locale/en-US'
import ru from 'date-fns/locale/ru'
import { getLocale } from './utils/i18n'
import config from './config'

export const capitalize = value => {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
}

export const date = (value, dateFormat, locale) => {
  if (!value) {
    return ''
  }

  if (typeof (value) === 'string') {
    if (value.indexOf('T') === -1) {
      value += 'T00:00:00'
    }

    value = parseISO(value)
  }

  return format(value, dateFormat, { locale: !locale || locale === 'ru' ? ru : en })
}

export const duration = (value, locale) => {
  const parts = value.split(':')
  value = {
    hours: parseInt(parts[0]),
    minutes: parseInt(parts[1]),
    seconds: parts.length > 2 ? parseInt(parts[2]) : 0
  }
  return formatDuration(value, { format: ['hours', 'minutes'], zero: true, locale: !locale || locale === 'ru' ? ru : en })
}

export const intcomma = (value, removeZeros) => {
  // remove float zeros, if necessary
  if (typeof (removeZeros) !== 'undefined' && removeZeros) {
    value = parseFloat(value)
    if (value % 1 === 0) {
      value = parseInt(value)
    }
  }

  value += ''
  const x = value.split('.')
  let x1 = x[0]
  const x2 = x.length > 1 ? '.' + x[1] : ''
  const rgx = /(\d+)(\d{3})/

  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1 $2')
  }
  return x1 + x2
}

export const lower = value => {
  return (value || '').toLowerCase()
}

export const phone = value => {
  value = '' + value
  if (value.indexOf('tel:') === 0) {
    value = value.split('tel:')[1]
  }
  value = value.replace('доб', ',')
  value = value.replace('ext', ',')

  return value.replace(/[^\d,]/g, '')
}

export const phoneUrl = value => value ? `tel:${value[0] !== '8' ? '+' : ''}` + phone(value) : ''

export const removeNewlines = value => (value || '').replace(/(\r\n\t|\n|\r\t)/gm, '')

export const uppercase = value => {
  if (!value) return ''
  return value.toString().toUpperCase()
}

export const url = value => {
  const type = typeof (value)
  let result = value

  if (type === 'object') {
    result = Object.assign({}, value)
    let locale = (result.params || {}).locale || null
    if (!locale) {
      locale = getLocale()
    }

    if (locale && locale === config.locales[0]) {
      delete result.params.locale
    } else if (locale && locale !== config.locales[0]) {
      if (result.name === 'index') {
        result.name = 'index-' + locale
      } else {
        if (result.name.search(/^locale-/) < 0) {
          result.name = 'locale-' + result.name
          result.params.locale = locale
        }
      }
    }
  } else if (type === 'string') {
    const parts = result.toLowerCase().split('/')
    if (parts.length > 1 && parts[1].length === 2 && parts[1] === config.locales[0]) {
      parts.splice(1, 1)
      result = parts.join('/')
    }
  }

  if (!result) {
    result = '/'
  }
  return result
}

export default {
  capitalize,
  date,
  intcomma,
  lower,
  phone,
  phoneUrl,
  removeNewlines,
  uppercase,
  url
}
