import Vue from 'vue'
import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'

if (process.server || (process.browser && window.location.hostname !== 'localhost' && window.location.hostname !== '127.0.0.1')) {
  Raven
    .config('https://234ab9db953f40bbac3c935e8423a6b6@sentry.facedev.ru/51')
    .addPlugin(RavenVue, Vue)
    .install()
}
