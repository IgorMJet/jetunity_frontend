import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

if (!Object.entries) {
  Object.entries = function (obj) {
    var ownProps = Object.keys(obj)
    var i = ownProps.length
    var resArray = new Array(i) // preallocate the Array
    while (i--) { resArray[i] = [ownProps[i], obj[ownProps[i]]] }

    return resArray
  }
}

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCQMoy0cEsBra9FVSFKfpuSu1qOCruB1KI',
    region: 'EN',
    language: 'en'
  }
})
Vue.component('v-map', VueGoogleMaps.Map)
Vue.component('v-marker', VueGoogleMaps.Marker)
Vue.component('v-info-window', VueGoogleMaps.InfoWindow)
