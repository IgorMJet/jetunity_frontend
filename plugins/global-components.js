import Vue from 'vue'

import AboutSection from '../components/sections/about-section/about-section'
import Alert from '../components/global/alert/alert'
import AppsSection from '../components/sections/apps-section/apps-section'
import BookingForm from '../components/forms/booking-form/booking-form'
import BookingForm2 from '../components/forms/booking-form/booking-form2'
import BookingFormThanks from '../components/sections/booking-form-thanks/booking-form-thanks'
import FlightBooking from '../components/forms/flight-booking/flight-booking'
import BrokerForm from '../components/forms/broker-form/broker-form'
import CookieBar from '../components/global/cookie-bar/cookie-bar'
import Dropdown from '../components/controls/dropdown/dropdown'
import FeaturesSection from '../components/sections/features-section/features-section'
import FooterGlobal from '../components/global/footer-block/footer-block'
import HeaderGlobal from '../components/global/header-block/header-block'
import HeroSection from '../components/sections/hero-section/hero-section'
import InfoBlock from '../components/global/info-block/info-block'
import Modal from '../components/global/modal/modal'
import Pagination from '../components/global/pagination/pagination'
import PartnerForm from '../components/forms/partner-form/partner-form'
import PartnersSection from '../components/sections/partners-section/partners-section'
import PartnersSliderSection from '../components/sections/partners-slider-section/partners-slider-section'
import SearchSection from '../components/sections/search-section/search-section'
import ServicesSection from '../components/sections/services-section/services-section'
import Services2Section from '../components/sections/services2-section/services2-section'
import Socials from '../components/global/socials/socials'
import Spinner from '../components/global/spinner/spinner'
import UserRegForm from '../components/forms/user-reg-form/user-reg-form'
import YearsSection from '../components/sections/years-section/years-section'

Vue.component('about-section', AboutSection)
Vue.component('alert', Alert)
Vue.component('apps-section', AppsSection)
Vue.component('booking-form', BookingForm)
Vue.component('booking-form2', BookingForm2)
Vue.component('booking-form-thanks', BookingFormThanks)
Vue.component('flight-booking', FlightBooking)
Vue.component('info-block', InfoBlock)
Vue.component('broker-form', BrokerForm)
Vue.component('cookie-bar', CookieBar)
Vue.component('dropdown', Dropdown)
Vue.component('features-section', FeaturesSection)
Vue.component('footer-block', FooterGlobal)
Vue.component('header-block', HeaderGlobal)
Vue.component('hero-section', HeroSection)
Vue.component('modal', Modal)
Vue.component('pagination', Pagination)
Vue.component('partner-form', PartnerForm)
Vue.component('partners-section', PartnersSection)
Vue.component('partners-slider-section', PartnersSliderSection)
Vue.component('search-section', SearchSection)
Vue.component('services-section', ServicesSection)
Vue.component('services2-section', Services2Section)
Vue.component('socials', Socials)
Vue.component('spinner', Spinner)
Vue.component('user-reg-form', UserRegForm)
Vue.component('years-section', YearsSection)
